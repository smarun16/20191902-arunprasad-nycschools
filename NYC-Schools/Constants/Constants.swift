//
//  Constants.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/19/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import Foundation
import UIKit

//App Name
let APP_NAME = "NYC Schools"

///----------------//////

//Shared Objects
let APPDELEGATE = (UIApplication.shared.delegate as? AppDelegate)
let HELPER = Helper()
let SERVICE = Service()

////--------------//////

//Service Url
let API_URL_NYC_SCHOOLS_LIST = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
let API_URL_SAT_SCORE = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
//Header
let CONTENT_TYPE = "Content-Type"
let CONNTENT_VALUE = "application/json"

//Body
let APP_TOKEN = "app_token"
let API_APP_TOCKEN = "xavT9njmqU81skj39Tgy4T7Ou"
let LIMIT = "limit"
let DBN = "dbn"

///----------------//////

//Alert
let ALERT_SEARCH_CANCEL = "Are you sure to cancel?"
let ALERT_NO_DATA_FOUND = "No data found"
let ALERT_SOMETHING_WENT_WRONG = "Sorry! Something went wrong. Please try again later"
let ALERT_NO_INTERNET = "Pease check your network connection"
let ALERT_MAIL = "Your device could not send e-mail.  Please check e-mail configuration and try again."
let ALERT_CALL = "Sorry! Unable to make a call at the moment. Please try again."
let ALERT_MAP = "Sorry! Unable to draw the route. Please try again."
let ALERT_WEB = "Sorry! Unable to Show the web Page"


///----------------//////

//Button Title
let CANCEL_BUTTON_TITLE = "Cancel"
let OK_BUTTON_TITLE = "Ok"

///----------------//////

//Color

let APP_PRIMARY_COLOR = "122531"
let APP_COLOR_BLUE = "33488B"
let APP_COLOR_YELLOW = "F0C32E"
let APP_COLOR_RED = "CF213E"
