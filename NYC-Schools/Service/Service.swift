//
//  Service.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/19/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import Foundation
import SystemConfiguration

class Service {
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func getNYCSchoolList(completionHandler:@escaping (( [Schools]) -> Void), errorHandler:@escaping ((Error) -> Void)) {
        

//        let aParameter = [
//            "app_token": "xavT9njmqU81skj39Tgy4T7Ou",
//            ] as [String : Any]

   //     let postData = try? JSONSerialization.data(withJSONObject: aParameter, options: [])

        let request = NSMutableURLRequest(url: URL(string: API_URL_NYC_SCHOOLS_LIST)!,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "get"
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in

            if (error != nil) {

                errorHandler(error!)
            }
            else {

                do {

                    let aObject = try JSONDecoder().decode([Schools].self, from: data!)
                    completionHandler(aObject)
                }
                catch {

                    errorHandler(error)
                    print(error._userInfo as Any)
                }
            }
        })

        dataTask.resume()
    }
    
    func getSatDataList(completionHandler:@escaping (( [SatDetails]) -> Void), errorHandler:@escaping ((Error) -> Void)) {
        
        
        //        let aParameter = [
        //            "app_token": "xavT9njmqU81skj39Tgy4T7Ou",
        //            ] as [String : Any]
        
        //     let postData = try? JSONSerialization.data(withJSONObject: aParameter, options: [])
        
        let request = NSMutableURLRequest(url: URL(string: API_URL_SAT_SCORE)!,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "get"
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                
                errorHandler(error!)
            }
            else {
                
                do {
                    
                    let aObject = try JSONDecoder().decode([SatDetails].self, from: data!)
                    completionHandler(aObject)
                }
                catch {
                    
                    errorHandler(error)
                    print(error._userInfo as Any)
                }
            }
        })
        
        dataTask.resume()
    }
}
