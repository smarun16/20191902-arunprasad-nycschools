//
//  DetailTableViewCell.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/21/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import UIKit
import MessageUI

class DetailTableViewCell: UITableViewCell, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var gTitleLabel: UILabel!
    @IBOutlet weak var gNeighborhoodLabel: UILabel!
    @IBOutlet weak var gAddressLabel: UILabel!
    @IBOutlet weak var gCountLabel: UILabel!
    @IBOutlet weak var gEmailButton: UIButton!
    @IBOutlet weak var gCallButton: UIButton!
    @IBOutlet weak var gDistanceButton: UIButton!
    @IBOutlet weak var gInfoButton: UIButton!
    @IBOutlet weak var gRatingLabel: UILabel!
    
    
    var gSchoolsObject: Schools!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        HELPER.roundCorner(for: gCallButton, radius: 8, borderColor: .clear)
        HELPER.roundCorner(for: gEmailButton, radius: 8, borderColor: .clear)
        HELPER.roundCorner(for: gDistanceButton, radius: 8, borderColor: .clear)

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //Button Methode
    @IBAction func emailButtonTapped(_ sender: Any) {
        
        setUpMailComposer()
    }
    @IBAction func callButtonTapped(_ sender: Any) {
        
        setupCallFunction()
    }
    @IBAction func distanceButtonTapped(_ sender: Any) {
        
        setupMapWithLocation()
    }
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        
        guard let url = URL(string: "https://www.\(gSchoolsObject.schoolWebSitre!)") else {
            return
        }
        UIApplication.shared.open(url, options: [:]) { (_ isSuccess) in
            
            if isSuccess == false {
                
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    
                    while let presentedViewController = topController.presentedViewController {
                        
                        topController = presentedViewController
                    }
                    UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_WEB) { (_ action) in
                        
                    }
                }
            }
        }
    }
    
    //MARK:- Helper
    func setupCellWithNYCSchoolData() {
        
        if gSchoolsObject != nil {
            setupUI()
        }
        gTitleLabel.text = gSchoolsObject.schoolName ?? ""
        gNeighborhoodLabel.text = "near \(gSchoolsObject.schoolNeighborhood ?? "")"
        gAddressLabel.text = gSchoolsObject.schoolLocation ?? ""
        gCountLabel.text = "\(gSchoolsObject.schoolTotalStudents ?? "0")"
        
        let attendanceRate: String =  gSchoolsObject.schoolAttendance_rate ?? "0"
        let newStr = String(attendanceRate.prefix(3))
        gRatingLabel.text = "\(newStr) %"
    }
    
    func setupUI() {
        
        if gSchoolsObject.schoolWebSitre == nil && gInfoButton != nil  {
           
            gInfoButton.removeFromSuperview()
        }
        if gSchoolsObject.schoolMobileNo == nil && gCallButton != nil {
            
            gCallButton.removeFromSuperview()
        }
        
        if gSchoolsObject.schoolEmail == nil && gEmailButton != nil {
            
            gEmailButton.removeFromSuperview()
        }
        
        if gSchoolsObject.schoolLat == nil &&  gSchoolsObject.schoolLong == nil && gDistanceButton != nil {

            gDistanceButton.removeFromSuperview()
        }
    }
    
    //MARK:Mail Setup and Methodes
    func setUpMailComposer() {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            if MFMailComposeViewController.canSendMail() {
                
                topController.present(mailComposeViewController, animated: true, completion: nil)
                
            }
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([gSchoolsObject.schoolEmail ?? ""])
        mailComposerVC.setSubject("Reg: \(gSchoolsObject.schoolName ?? "") Details")
        mailComposerVC.setMessageBody("Hi", isHTML: false)
        
        return mailComposerVC
    }
    
    private func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Phone setup and methode
    
    func setupCallFunction() {
        
        
        if gSchoolsObject.schoolMobileNo?.isEmpty == true {
            
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                
                while let presentedViewController = topController.presentedViewController {
                    
                    topController = presentedViewController
                }
                UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_CALL) { (_ action) in
                    
                }
            }
        }
        else {
            
            guard let url = URL(string: "tel://+1" + gSchoolsObject.schoolMobileNo!) else {// + 1 is set for US
                return
            }
            UIApplication.shared.open(url, options: [:]) { (_ isSuccess) in
                
                if isSuccess == false {
                    
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        
                        while let presentedViewController = topController.presentedViewController {
                            
                            topController = presentedViewController
                        }
                        UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_CALL) { (_ action) in
                            
                        }
                    }
                }
            }
        }
        
    }
    
    //MARK: Direction
    func setupMapWithLocation() {
        
        let directionsURL = "http://maps.apple.com/?\(gSchoolsObject.schoolLat!),\(gSchoolsObject.schoolLong!)"
        guard let url = URL(string: directionsURL) else {
            return
        }
        UIApplication.shared.open(url, options: [:]) { (_ isSuccess) in
            
            if isSuccess == false {
                
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    
                    while let presentedViewController = topController.presentedViewController {
                        
                        topController = presentedViewController
                    }
                    UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_MAP) { (_ action) in
                        
                    }
                }
            }
        }
    }
}
