//
//  ListTableViewCell.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/19/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import UIKit
import MessageUI

class ListTableViewCell: UITableViewCell, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var gTitleLabel: UILabel!
    @IBOutlet weak var gCityLabel: UILabel!
    @IBOutlet weak var gCountLabel: UILabel!
    @IBOutlet weak var gEmailButton: UIButton!
    @IBOutlet weak var gCallButton: UIButton!
    @IBOutlet weak var gDistanceButton: UIButton!
    @IBOutlet weak var gRatingLabel: UILabel!
    
    var gSchoolsObject: Schools!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        HELPER.roundCorner(for: gCallButton, radius: 8, borderColor: .clear)
        HELPER.roundCorner(for: gEmailButton, radius: 8, borderColor: .clear)
        HELPER.roundCorner(for: gDistanceButton, radius: 8, borderColor: .clear)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Button Methode
    @IBAction func emailButtonTapped(_ sender: Any) {
        
        setUpMailComposer()
    }
    @IBAction func callButtonTapped(_ sender: Any) {
        
        setupCallFunction()
    }
    @IBAction func distanceButtonTapped(_ sender: Any) {
       
       setupMapWithLocation()
    }
    
    //MARK:- Helper
    func setupCellWithNYCSchoolData() {
        
        if gSchoolsObject != nil {
            setupUI()
        }
        gTitleLabel.text = gSchoolsObject.schoolName ?? ""
        gCityLabel.text = gSchoolsObject.schoolCity ?? ""
        gCountLabel.text = "\(gSchoolsObject.schoolTotalStudents ?? "0")"
        
        let attendanceRate: String =  gSchoolsObject.schoolAttendance_rate ?? "0"
        let newStr = String(attendanceRate.prefix(3))
        gRatingLabel.text = "\(newStr) %"
        
       
    }
    
    func setupUI() {
        
        if gSchoolsObject.schoolMobileNo == nil {
            
            if gCallButton != nil {
                gCallButton.removeFromSuperview()
            }
        }
        
        if gSchoolsObject.schoolEmail == nil {
            
            if gEmailButton != nil {
                gEmailButton.removeFromSuperview()
            }
            
        }
        
        if gSchoolsObject.schoolLat == nil &&  gSchoolsObject.schoolLong == nil {
            
            if gDistanceButton != nil {
                gDistanceButton.removeFromSuperview()
            }
        }
//        if gSchoolsObject.schoolMobileNo != nil {
//
////            gCallButton.layoutIfNeeded()
////            gCallButton.isHidden = false
//        }
//        else {
//            gCallButton.removeFromSuperview()
//            //gCallButton.isHidden = true
//        }
//
//        if gSchoolsObject.schoolEmail != nil {
//
//            //gEmailButton.layoutIfNeeded()
//           // gEmailButton.isHidden = false
//
//        }
//        else {
//
//            if gEmailButton != nil {
//                gEmailButton.removeFromSuperview()
//            }
//        }
//
//        if gSchoolsObject.schoolLat != nil &&  gSchoolsObject.schoolLong != nil {
//
////            gDistanceButton.layoutIfNeeded()
////            gDistanceButton.isHidden = false
//        }
//        else {
//            gDistanceButton.removeFromSuperview()
//            //gDistanceButton.isHidden = true
//        }
    }
    //MARK:Mail Setup and Methodes
    func setUpMailComposer() {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
          
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }

            if MFMailComposeViewController.canSendMail() {
                
                topController.present(mailComposeViewController, animated: true, completion: nil)
                
            }
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self 
        mailComposerVC.setToRecipients([gSchoolsObject.schoolEmail ?? ""])
        mailComposerVC.setSubject("Reg: \(gSchoolsObject.schoolName ?? "") Details")
        mailComposerVC.setMessageBody("Hi", isHTML: false)
        
        return mailComposerVC
    }
    
    private func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
       
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Phone setup and methode
    
    func setupCallFunction() {
        
        if gSchoolsObject.schoolMobileNo?.isEmpty == true {
            
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
               
                while let presentedViewController = topController.presentedViewController {
                 
                    topController = presentedViewController
                }
                UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_CALL) { (_ action) in
                    
                }
            }
        }
        else {
            
            guard let url = URL(string: "tel://+1" + gSchoolsObject.schoolMobileNo!) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:]) { (_ isSuccess) in
                
                if isSuccess == false {
                    
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        
                        while let presentedViewController = topController.presentedViewController {
                            
                            topController = presentedViewController
                        }
                        UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_CALL) { (_ action) in
                            
                        }
                    }
                }
            }
        }
        
    }
    
    //MARK: Direction
    func setupMapWithLocation() {
        
        let directionsURL = "http://maps.apple.com/?\(gSchoolsObject.schoolLat!),\(gSchoolsObject.schoolLong!)"
        guard let url = URL(string: directionsURL) else {
            return
        }
        UIApplication.shared.open(url, options: [:]) { (_ isSuccess) in
            
            if isSuccess == false {
                
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    
                    while let presentedViewController = topController.presentedViewController {
                        
                        topController = presentedViewController
                    }
                    UIAlertController().showAlertViewWithOkAction(topController, title: "", message: ALERT_MAP) { (_ action) in
                        
                    }
                }
            }
        }
    }
}

