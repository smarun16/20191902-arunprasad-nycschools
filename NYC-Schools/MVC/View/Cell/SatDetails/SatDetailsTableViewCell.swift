//
//  SatDetailsTableViewCell.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/22/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import UIKit

class SatDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var gTestTakersLabel: UILabel!
    @IBOutlet weak var gMathAverageLabel: UILabel!
    @IBOutlet weak var gWritingAvgLabel: UILabel!
    @IBOutlet weak var gCriticalAvgLabel: UILabel!
    
    
    var gSatDetailsObject: SatDetails!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Helper
    func setupCellWithSatData() {
        
        gTestTakersLabel.text = gSatDetailsObject.satTestTakere ?? ""
        gMathAverageLabel.text = gSatDetailsObject.satMathAvgScore ?? ""
        gWritingAvgLabel.text = gSatDetailsObject.satCriticalReadingAvgScore ?? ""
        gCriticalAvgLabel.text = gSatDetailsObject.satCriticalReadingAvgScore ?? ""
       
    }
}
