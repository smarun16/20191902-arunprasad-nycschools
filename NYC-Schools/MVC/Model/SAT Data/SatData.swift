//
//  SatData.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/19/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import Foundation

struct SatDetails: Codable {

    var satdbn: String?
    var satTestTakere: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritingAvgScore: String?
    var satSchoolName: String?

    enum CodingKeys: String, CodingKey {
        
        case satdbn = "dbn"
        case satTestTakere = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case satSchoolName = "school_name"
    }
}
