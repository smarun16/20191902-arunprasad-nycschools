//
//  DetailViewController.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/21/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    //outlets
    @IBOutlet weak var tableView: UITableView!
    
    //Constants
    
    let Header = ""
    let ExtracurricularActivites = "Extra CurricularActivites"
    let Academicopportunities = "Academic Opportunities"
    let Interest = "Interest"
    let OverViewParagraph = "OverView Paragraph"
    let SchoolSports = "School Sports"
    let SATDetails = "Sat Details"

    
    //Model
    var gSchoolsObject : Schools!
    var satDataObject = SatDetails()
    var tableInfoArray = [String]()
    var tableDataInfoArray = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpUI()
        setUpModel()
    }
    
    //MARK: Setup Methode
    func setUpUI() {
        
        title = "Details"
        setupLeftBarButton()
        setupTableView()
    }
    
    func setUpModel() {
        
        getSatData()
    }
    
    func setupTableView() {
        
        tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
        tableView.register(UINib(nibName: "SatDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "SatDetailsTableViewCell")

        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 160
        tableView.contentInset = UIEdgeInsets.init(top: -30, left: 0, bottom: 0, right: 0)
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
    }
    
    func setupLeftBarButton() {
        
        let aBarButton: UIBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "icon_filter"), style: .plain, target: self, action: #selector(leftBarButtonTapped))
        self.navigationController?.navigationItem.leftBarButtonItem = aBarButton
    }
    
    //MARK: UIButton Methode
    @objc func leftBarButtonTapped() {
        
    }
    
    //MARK: Service
    func getSatData() {
        
        if SERVICE.isConnectedToNetwork() == false {
            
            UIAlertController().showAlertViewWithOkAction(self, title: APP_NAME, message: ALERT_NO_INTERNET) { (_ action) in
            }
            return
        }
        
        HELPER.showLoadingIndicator()
        SERVICE.getSatDataList(completionHandler: { (_ aSatDataObject) in
            
            HELPER.hideLoadingIndicator()
            if aSatDataObject.count != 0 {
                
                DispatchQueue.main.async {
                    
       
                    var aSatDetailArray = aSatDataObject
                    aSatDetailArray = aSatDataObject.filter { $0.satdbn == self.gSchoolsObject.schooldbn}
                    
                    if aSatDetailArray.count != 0 {
                        self.satDataObject = aSatDetailArray[0]
                    }
                    self.reloadTabelWithdata()
                }
            }
            else {
                
                UIAlertController().showAlertViewWithOkAction(self, title: APP_NAME, message: ALERT_NO_DATA_FOUND) { (_ action) in
                    
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }) { (_ error) in
            
            UIAlertController().showAlertViewWithOkAction(self, title: APP_NAME, message: ALERT_SOMETHING_WENT_WRONG) { (_ action) in
                
                HELPER.hideLoadingIndicator()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK: Helper
    
    func reloadTabelWithdata() {
        
        loadTableViewHeaderData()
    }
    
    func loadTableViewHeaderData() {
        
        tableInfoArray += [Header]
        
        if gSchoolsObject.schoolOverView != nil {
            
            tableInfoArray += [OverViewParagraph]
        }
        if gSchoolsObject.schoolExtracurricularActivites != nil {
           
            tableInfoArray += [ExtracurricularActivites]
        }
       
        if gSchoolsObject.schoolSports != nil {
            
            tableInfoArray += [SchoolSports]
        }
        
        if gSchoolsObject.schoolAcademicopportunities1 != nil || gSchoolsObject.schoolAcademicopportunities2 != nil || gSchoolsObject.schoolAcademicopportunities3 != nil || gSchoolsObject.schoolAcademicopportunities4 != nil || gSchoolsObject.schoolAcademicopportunities5 != nil  {
            
            tableInfoArray += [Academicopportunities]
            setupAcademicOpportunity()
        }
        if gSchoolsObject.schoolInterest1 != nil || gSchoolsObject.schoolInterest2 != nil || gSchoolsObject.schoolInterest3 != nil || gSchoolsObject.schoolInterest4 != nil || gSchoolsObject.schoolInterest5 != nil || gSchoolsObject.schoolInterest6 != nil || gSchoolsObject.schoolInterest7 != nil || gSchoolsObject.schoolInterest8 != nil {
            
            tableInfoArray += [Interest]
            setupIntrest()
        }
        
        if satDataObject.satdbn != nil {
            
            tableInfoArray += [SATDetails]
        }
        
        UIView.transition(with: self.tableView, duration: 1.0, options: .transitionCrossDissolve, animations: {
            
            self.tableView.reloadData()
        }, completion: nil)
    }
    
    func setupAcademicOpportunity() {
        
        var aAcademicopportunities: [String] = [""]

        if gSchoolsObject.schoolAcademicopportunities1 != nil {
            
            aAcademicopportunities = [gSchoolsObject.schoolAcademicopportunities1!]
        }
        if gSchoolsObject.schoolAcademicopportunities2 != nil {
            aAcademicopportunities = [gSchoolsObject.schoolAcademicopportunities2!]
        }
        if gSchoolsObject.schoolAcademicopportunities3 != nil {
            aAcademicopportunities = [gSchoolsObject.schoolAcademicopportunities3!]
        }
        if  gSchoolsObject.schoolAcademicopportunities4 != nil {
            aAcademicopportunities = [gSchoolsObject.schoolAcademicopportunities4!]
        }
        if  gSchoolsObject.schoolAcademicopportunities5 != nil  {
            aAcademicopportunities = [gSchoolsObject.schoolAcademicopportunities5!]
        }
        
        tableDataInfoArray = [
            [Academicopportunities: aAcademicopportunities],
        ]
    }
    func setupIntrest() {

        var aInterest: [String] = [""]

        if gSchoolsObject.schoolInterest1 != nil {
            aInterest = [gSchoolsObject.schoolInterest1!]
        }
        if gSchoolsObject.schoolInterest2 != nil {
            aInterest = [gSchoolsObject.schoolInterest2!]
        }
        if gSchoolsObject.schoolInterest3 != nil {
            aInterest = [gSchoolsObject.schoolInterest3!]
        }
        if gSchoolsObject.schoolInterest4 != nil {
            aInterest = [gSchoolsObject.schoolInterest4!]
        }
        if gSchoolsObject.schoolInterest5 != nil {
            aInterest = [gSchoolsObject.schoolInterest5!]
        }
        if gSchoolsObject.schoolInterest6 != nil {
            aInterest = [gSchoolsObject.schoolInterest6!]
        }
        if gSchoolsObject.schoolInterest7 != nil {
            aInterest = [gSchoolsObject.schoolInterest7!]
        }
        if gSchoolsObject.schoolInterest8 != nil {
            aInterest = [gSchoolsObject.schoolInterest8!]
        }
        
        tableDataInfoArray += [[Interest: aInterest]]
    }
}


extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return tableInfoArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        switch tableInfoArray[section] {
//        case Header, OverViewParagraph, SchoolSports, ExtracurricularActivites, SchoolSports:
//            return 1
        case Academicopportunities:
            return (tableDataInfoArray[0][Academicopportunities] as? [String])?.count ?? 0
        case Interest:
            return (tableDataInfoArray[1][Interest] as? [String])?.count ?? 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let aLabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 35))
        aLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        aLabel.text = "  \(tableInfoArray[section]) :"
        return aLabel
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return (section == 0) ? 0 : 35
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableInfoArray[indexPath.section] {
        case Header:
            
            let aCell: DetailTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell") as? DetailTableViewCell
            
            if aCell == nil {
                
                tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
            }
            aCell?.gSchoolsObject = gSchoolsObject
            aCell?.setupCellWithNYCSchoolData()
            return aCell!

        case OverViewParagraph:
            
            let aCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            aCell.textLabel?.text = gSchoolsObject.schoolOverView
            aCell.textLabel?.numberOfLines = 0
            
            return aCell
        case ExtracurricularActivites:
            let aCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            aCell.textLabel?.text = gSchoolsObject.schoolExtracurricularActivites
            aCell.textLabel?.numberOfLines = 0
            
            return aCell
        case SchoolSports:
            let aCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            aCell.textLabel?.text = gSchoolsObject.schoolSports
            aCell.textLabel?.numberOfLines = 0
            
            return aCell
        case Academicopportunities:
            let aCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            aCell.textLabel?.text = (tableDataInfoArray[0][Academicopportunities] as? [String])?[indexPath.row]
            aCell.textLabel?.numberOfLines = 0
            
            return aCell
        case Interest:
            let aCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            aCell.textLabel?.text = (tableDataInfoArray[1][Interest] as? [String])?[indexPath.row]
            aCell.textLabel?.numberOfLines = 0
            
            return aCell
        default:
            let aCell: SatDetailsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "SatDetailsTableViewCell") as? SatDetailsTableViewCell
            
            aCell?.gSatDetailsObject = satDataObject
            aCell?.setupCellWithSatData()
            return aCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
