//
//  ViewController.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/19/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //outlets
    @IBOutlet weak var tableView: UITableView!

    //Model
    var schoolsArray = [Schools]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpUI()
        setUpModel()
    }
    
    //MARK: Setup Methode
    func setUpUI() {
        
        setupLeftBarButton()
        setupTableView()
    }
    
    func setUpModel() {
        
        getNYCSchoolsData()
    }

    func setupTableView() {
        
        tableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "ListTableViewCell")
       
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func setupLeftBarButton() {
        
        let aBarButton: UIBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "icon_filter"), style: .plain, target: self, action: #selector(leftBarButtonTapped))
        self.navigationController?.navigationItem.leftBarButtonItem = aBarButton
    }
    
    //MARK: UIButton Methode
    @objc func leftBarButtonTapped() {
        
    }
    
    //MARK: Service
    func getNYCSchoolsData() {
        
        if SERVICE.isConnectedToNetwork() == false {
            
            UIAlertController().showAlertViewWithOkAction(self, title: APP_NAME, message: ALERT_NO_INTERNET) { (_ action) in
            }
            return
        }
        
        HELPER.showLoadingIndicator()
        SERVICE.getNYCSchoolList(completionHandler: { (_ aSchoolArrayObj) in
            
            HELPER.hideLoadingIndicator()
            if aSchoolArrayObj.count != 0 {
                
                DispatchQueue.main.async {
                    
                    self.schoolsArray = aSchoolArrayObj
                    self.reloadTabelWithdata()
                }
            }
            else {
                
                UIAlertController().showAlertViewWithOkAction(self, title: APP_NAME, message: ALERT_NO_DATA_FOUND) { (_ action) in
                    
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }) { (_ error) in
            
            UIAlertController().showAlertViewWithOkAction(self, title: APP_NAME, message: ALERT_SOMETHING_WENT_WRONG) { (_ action) in
                
                HELPER.hideLoadingIndicator()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK: Helper
    
    func reloadTabelWithdata() {
        
        DispatchQueue.main.async {
            
            UIView.transition(with: self.tableView, duration: 1.0, options: .transitionCrossDissolve, animations: {
                
                self.tableView.reloadData()
            }, completion: nil)
        }
       
    }
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return  schoolsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let aCell: ListTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as? ListTableViewCell
        
//        if aCell == nil {
//
//            tableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "ListTableViewCell")
//        }
        
        aCell?.gSchoolsObject = schoolsArray[indexPath.row]
        aCell?.setupCellWithNYCSchoolData()
        return aCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let aViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController
        aViewController?.gSchoolsObject = schoolsArray[indexPath.row]
        self.navigationController?.pushViewController(aViewController!, animated: true)
    }
}
