//
//  Helper.swift
//  NYC-Schools
//
//  Created by Arunprasat Selvaraj on 2/19/19.
//  Copyright © 2019 Arunprasat Selvaraj. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    
    //MARK - View
    func roundCorner(for view: UIView, radius: Float, borderColor color: UIColor) {
        
        view.layer.cornerRadius = CGFloat(radius)
        view.layer.borderWidth = 1
        view.layer.borderColor = color.cgColor
        view.clipsToBounds = true
    }
    
    func showLoadingIndicator() {
        
        let aView =  UINib(nibName: "LoadingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller
            topController.view.addSubview(aView)
            aView.frame = topController.view.frame //superview?.bounds ?? topController.view.bounds
            aView.tag = 111
            topController.view.bringSubviewToFront(aView)
        }
    }
    
    func hideLoadingIndicator() {
        
        DispatchQueue.main.async {
            
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                topController.view.viewWithTag(111)?.removeFromSuperview()
            }
        }
        
    }
}
